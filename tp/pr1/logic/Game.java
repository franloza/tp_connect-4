package tp.pr1.logic;

public class Game {
	
	//Constants
	public static final byte WIDTH = 7;
	public static final byte HEIGHT = 6;
	public static final byte MAX_UNDO = 10;	
	
	//Constructor
	
	public Game() {
		this.board = new Board(WIDTH,HEIGHT);
		this.finished = false;
		this.winner = Counter.EMPTY;
		this.turn = Counter.WHITE;
		this.undoStack = new int[MAX_UNDO];
		for (int i = 0; i < MAX_UNDO; ++i)
			this.undoStack[i] = 0;
		this.numUndo = 0;
	}
	
	public void reset() {
		this.board.reset();
		this.finished = false;
		this.winner = Counter.EMPTY;
		this.turn = Counter.WHITE;
		for (int i = 0; i < MAX_UNDO; ++i) //This could be skipped anyway
			this.undoStack[i] = 0;
		this.numUndo = 0;
	}
	
	public boolean executeMove(Counter colour, int col) {
		boolean b;
		
		if (col >= 1 && col <= board.getWidth() && board.topCounter(col) > 1  && turn == colour &&  finished == false) {
			b = true;
			board.setPosition(col, board.topCounter(col) - 1, colour);
			checkWin();
			//check4(colour, col);
			if (turn == Counter.WHITE)
				turn = Counter.BLACK;				
			else
				turn = Counter.WHITE;
			if(numUndo < MAX_UNDO) {
				undoStack[numUndo] = col;
				++numUndo;
			}
			else { //This allows to renew the array when it's full one by one
				for (int i = 0; i < MAX_UNDO - 1; i++) {
					undoStack[i] = undoStack[i+1];						
				}		
				undoStack[MAX_UNDO - 1] = col;
			}			
		}
		else
			b = false;
		
		return b;
	}
	
	/*private void check4(Counter colour, int col) {
		boolean equal = true, limitReached = false;
		int i = col, j = board.topCounter(col), points = 0, vertical = 0, horizontal = 0, iteration = 0;
		
		while (iteration < 8 && points < 3) {
			do {
				if (iteration == 0) //Right
					++horizontal;
				else if (iteration == 1) //Left
					--horizontal;
				else if (iteration == 2) //Down
					++vertical;
				else if (iteration == 3) //Up
					--vertical;
				else if (iteration == 4) { //
					++horizontal;
					++vertical;
				}
				else if (iteration == 5) {
					--horizontal;
					--vertical;
				}
				else if (iteration == 6) {
					++horizontal;
					--vertical;
				}
				else if (iteration == 7) {
					--horizontal;
					++vertical;
				}
				if (i + horizontal > board.getHeight() || i + horizontal < 1 || j + vertical > board.getWidth() || j + vertical < 1)
					limitReached = true;
				else if (board.getPosition(i, j) == board.getPosition(i + horizontal,j + vertical)) {
					++points;
				}
				else
					equal = false;
			} while(equal && points < 4 && !limitReached);
			if (iteration % 2 == 1 && points < 3)
				points = 0;
			equal = true;
			limitReached = false;
			horizontal = 0;
			vertical = 0;
			++iteration;
		}
		if (points >= 3)
			finished = true;
		i = 1;
		while (board.topCounter(i) == 1 && i <= board.getWidth()) {
			++i;
		}
		if (i > board.getWidth())
			finished = true;
	}*/
	
	private void checkWin () {
		int i = 0, j = 0;
		Counter firstTile;
		//Horizontal check
		while (i < board.getHeight() && !finished) {
			while (j < board.getWidth() - 3 && !finished) {
				firstTile = board.getPosition(j+1, i+1);
				if (firstTile != Counter.EMPTY) {
					if (firstTile == board.getPosition(j+2, i+1) ) {
						if (firstTile == board.getPosition(j+3, i+1) ) {
							if (firstTile == board.getPosition(j+4, i+1) ) {	
								finished = true;
							}		
						}												
					}
				}
				j++;
			}			
			i++;
			j=0;
		}	
		
		//Vertical check
		i = 0; j = 0;
		while (i < board.getWidth() && !finished) {
			while (j < board.getHeight() - 3 && !finished) {
				firstTile = board.getPosition(i+1, j+1);
				if (firstTile != Counter.EMPTY) {
					if (firstTile == board.getPosition(i+1, j+2) ) {
						if (firstTile == board.getPosition(i+1, j+3) ) {
							if (firstTile == board.getPosition(i+1, j+4) ) {	
								finished = true;
							}		
						}												
					}
				}
				j++;
			}			
			i++;
			j=0;
		}
		
		
		//Diagonal check
		i = 0; j = 0;
		while (i < board.getWidth() -3 && !finished) {
			while (j < board.getHeight()-3 && !finished) {
				firstTile = board.getPosition(i+1, j+1);
				if (firstTile != Counter.EMPTY) {	
					if (firstTile == board.getPosition(i+2,j+2) ) {
						if (firstTile == board.getPosition(i+3,j+3) ) {
							if (firstTile == board.getPosition(i+4,j+4) ) {	
								finished = true;
							}		
						}												
					}
				}
				j++;
			}			
			i++;
			j=0;
		}	
		
		//Inverted Diagonal check
		i = 3; j = 0;
		while (i < board.getWidth() && !finished) {
			while (j < board.getHeight() - 3 && !finished) {
				firstTile = board.getPosition(i+1, j+1);
				if (firstTile != Counter.EMPTY) {	
					if (firstTile == board.getPosition(i,j+2) ) {
						if (firstTile == board.getPosition(i-1,j+3) ) {
							if (firstTile == board.getPosition(i-2,j+4) ) {	
								finished = true;
							}		
						}												
					}
				}
				j++;
			}			
			i++;
			j=0;
		}	
		
		//Draw game check
		i = 1;
		while (board.topCounter(i) == 1 && i <= board.getWidth()) {
			++i;
		}
		if (i > board.getWidth()) {
			finished = true;
			winner = Counter.EMPTY;
		}
		else if (finished)
			winner = turn;
	}
	
	public boolean undo() {
		boolean undo;
		if (numUndo == 0) {
			undo = false;
		}
		else {
			undo = true;
			int undo_column = this.undoStack[this.numUndo - 1];
			int undo_row = board.topCounter(undo_column);
			board.setPosition(undo_column, undo_row, Counter.EMPTY);
			--this.numUndo;
			//Changes the turn
			if(turn == Counter.WHITE) {
				turn = Counter.BLACK;
			}
			else {
				turn = Counter.WHITE;
			}
		}	
		return undo;
	}
	
	public Counter getTurn() {
		return turn;
	}
	
	public Counter getWinner() {
		return winner;
	}
	
	public boolean isFinished() {		
		return finished;
	}
	
	public Board getBoard() {
		return board;
	}
	
	public static String strTurn (Counter turn) {
		String turnStr;
		if (turn == Counter.WHITE){
			turnStr = "White";
		}
		else {
			turnStr = "Black";
		}
		return turnStr;
	}
	
	private Board board;
	private Counter turn;
	private boolean finished;
	private Counter winner;
	private int[] undoStack;
	private int numUndo;



}
