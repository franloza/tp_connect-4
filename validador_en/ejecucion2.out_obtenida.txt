|       |
|       |
|       |
|       |
|       |
|       |
+-------+
 1234567

White to move
Please enter a command: Please provide the column number: |       |
|       |
|       |
|       |
|       |
|   O   |
+-------+
 1234567

Black to move
Please enter a command: Please provide the column number: Invalid move, please try again
|       |
|       |
|       |
|       |
|       |
|   O   |
+-------+
 1234567

Black to move
Please enter a command: Game restarted
|       |
|       |
|       |
|       |
|       |
|       |
+-------+
 1234567

White to move
Please enter a command: Nothing to undo, please try again
|       |
|       |
|       |
|       |
|       |
|       |
+-------+
 1234567

White to move
Please enter a command: Closing the game...
