package tp.pr1.control;

import java.util.Scanner;

import tp.pr1.logic.*;
public class Controller {
	
	//Attributes
	
	private Game game;
	private Scanner in;
	
	//Constructor
	
	public Controller (Game p, java.util.Scanner in) {
		this.game = p;
		this.in = in;
	}
	
	//Methods
	
	public void run() {
		boolean exit = false;
		String command;
		Counter turn;
		while (!this.game.isFinished() && !exit) {
				System.out.println (this.game.getBoard().toString());
				turn = game.getTurn();						
				System.out.println (Game.strTurn(turn) + " to move");
				System.out.print ("Please enter a command: ");
				command = in.nextLine();
				command = command.toLowerCase(); //Converts the command to lower case
				switch (command) {
					case ("make a move"):
					{
						int column;
						boolean validColumn;
						System.out.print ("Please provide the column number: ");
						column = in.nextInt();
						in.nextLine();
						validColumn = this.game.executeMove(this.game.getTurn(), column);
						if (!validColumn) {
								System.out.println ("Invalid move, please try again");
						}
						else if (this.game.isFinished()){
							System.out.println (this.game.getBoard().toString());
							System.out.println ("Game over. " + Game.strTurn(game.getWinner()) + " wins");
							System.out.print("Closing the game...");
						}
					}
					break;	
					case ("undo"):
					{
						boolean undo;
						undo = this.game.undo();
						if (!undo) {
							System.out.println ("Nothing to undo, please try again");
						}						
					}
					break;	
					case ("restart"):
					{
						this.game.reset();
						System.out.println ("Game restarted");							
					}
					break;	
					case ("exit"):
					{
						exit = true;
						System.out.println ("Closing the game...");							
						
					}
					break;
					default: {
						System.err.println ("incorrect command: command not understood,please try again");
					}
			}	
		}
	}
}