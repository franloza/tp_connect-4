package tp.pr1.logic;

public class Board extends java.lang.Object {
	
	//Attributes
	private Counter [][] board;
	private int width;
	private int height;
	
	
	//Constructor
	public Board (int tx, int ty){
		if (tx < 1 || ty < 1) {
			tx = 1;
			ty = 1;
		}
		this.width = tx;
		this.height = ty;	
		this.board = new Counter [tx][ty];
		reset();
	}
	
	//Methods
	
	public int getHeight() {
		return this.height;
	}
	
	public int getWidth() {
		return this.width;
	}	
	
	public Counter getPosition(int x, int y) {
		if (x < 1 || y < 1 || x > this.width || y > this.height)
			return Counter.EMPTY;
		else
			return this.board[x - 1][y - 1];
	} 

	public void setPosition(int x, int y, Counter color)  {
		this.board[x - 1][y - 1] = color;
	}
	public void reset () {
		for (int j = 1; j <= this.width ; j++) {
			for (int i = 1; i <= this.height ; i++)
				setPosition(j, i, Counter.EMPTY);	
		}
	}
	public String toString () {
		String boardStr = "";
		for (int i = 1; i <= this.height ; i++) {
			boardStr = boardStr + "|";
			for (int j = 1; j <= this.width ; j++) {
				if (getPosition(j, i) == Counter.BLACK) {
					boardStr = boardStr + "X";
				}
				else if (getPosition(j, i) == Counter.WHITE) {
					boardStr = boardStr + "O";
				}
				else {
					boardStr = boardStr + " ";
				}
			}
			boardStr = boardStr + "|";
			boardStr = boardStr + "\n";
		}
		
		boardStr = boardStr + "+";
		for (int i = 1; i <= this.width; i++)
			boardStr = boardStr + "-";
		boardStr = boardStr + "+\n";
		
		boardStr = boardStr + " ";
		
		for (int j = 1; j <= this.width; j++)
			boardStr = boardStr + j;

		boardStr = boardStr + "\n";
		return boardStr;
	}

	public int topCounter (int column) {
		int row = 1;
		while (row <= getHeight() && (getPosition(column, row) ==  Counter.EMPTY)) {
			++row;			
		}
		
		return row; //row = 1 means the column it's full
	} //Given a certain column, returns the top counter on it (Row > height if it is full)
}