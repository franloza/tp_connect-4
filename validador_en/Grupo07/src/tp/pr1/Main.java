package tp.pr1;

import tp.pr1.control.*;
import tp.pr1.logic.*;
import java.util.Scanner;

public class Main {
	public static void main (String args []) {
		Game game = new Game();
		Scanner in = new Scanner (System.in);
		Controller control = new Controller (game, in);
		control.run();
	}
}